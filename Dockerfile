FROM alpine:latest

# install openntp
RUN apk update && \
    apk upgrade && \
    apk add --no-cache openntpd bash

# enable copying the enviroment variables to config
COPY  ./entrypoint.sh /


# ntp port
EXPOSE 123/udp 

# start ntpd in the foreground
ENTRYPOINT ["./entrypoint.sh"]
