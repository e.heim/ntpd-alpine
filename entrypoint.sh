#!/bin/bash

grep -v server /etc/ntpd.conf > /etc/ntpd2.conf && mv /etc/ntpd2.conf /etc/ntpd.conf

#if within the container the enviroment variable 'server1' is set, append it to ntpd.conf
# else set default 0.de.pool.ntp.or
if [ -z "$SERVER1" ]; then
  SERVER1="1.de.pool.ntp.org"
fi

# if within the container the enviroment variable 'server2' is set, append it to ntpd.conf.
# else set default 1.de.pool.ntp.org 
if [ -z "$SERVER2" ]; then
  SERVER2="2.de.pool.ntp.org"
fi

#if within the container the enviroment variable 'server3' is set, append it to ntpd.conf
# else set default 2.de.pool.ntp.org
if [ -z "$SERVER3" ]; then
  SERVER3="3.de.pool.ntp.org"
fi

echo "server $SERVER1" >> /etc/ntpd.conf
echo "server $SERVER2" >> /etc/ntpd.conf
echo "server $SERVER3" >> /etc/ntpd.conf
echo "listen on *" >> /etc/ntpd.conf

/usr/sbin/ntpd -v -d -s